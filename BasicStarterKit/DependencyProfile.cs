﻿using BasicStarterKit.Context;
using BasicStarterKit.Repositories;
using BasicStarterKit.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicStarterKit
{
    public static class DependencyProfile
    {
        public static void DependencyLoad(this IServiceCollection services)
        {
            //services.AddScoped(p => new ApplicationDbContext(p.GetService<DbContextOptions<ApplicationDbContext>>()));
            services.AddSingleton<IDbContext, ApplicationDbContext>();

            // repository and services 
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductService, ProductService>();
        }
    }
}
