﻿using AutoMapper;
using BasicStarterKit.Models;
using BasicStarterKit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicStarterKit
{
    public class MapperProfile
        : Profile
    {
        public MapperProfile()
        {
            CreateMap<ProductViewModel, Product>();
            CreateMap<Product, ProductViewModel>();
        }
    }
}
