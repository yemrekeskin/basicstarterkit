﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicStarterKit.Context
{
    public interface IDbContext
    {
        DbSet<TModel> Set<TModel>() where TModel : class;

        int SaveChanges();
    }
}
