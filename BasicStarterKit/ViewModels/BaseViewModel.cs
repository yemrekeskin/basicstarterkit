﻿using BasicStarterKit.Common;
using BasicStarterKit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicStarterKit.ViewModels
{
    public class BaseViewModel
    {
        public string UniqueKey { get; set; }

        public bool IsDeleted { get; set; }

        public ActivePassive ActivePassive { get; set; }
        public int Id { get; set; }
    }
}
