﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BasicStarterKit.Services
{
    public interface IService<TModel>
    {
        TModel Add(TModel model);

        TModel Update(TModel model);

        void Remove(TModel model);
        void Remove(long Id);

        TModel Get(TModel model);
        TModel Get(long Id);

        IEnumerable<TModel> List();
        IEnumerable<TModel> List(Expression<Func<TModel, bool>> predicate);
    }
}
