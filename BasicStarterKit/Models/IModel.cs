﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicStarterKit.Models
{
    public interface IModel
        : IModel<int>
    {

    }

    public interface IModel<T>
    {
        T Id { get; set; }
    }
}
