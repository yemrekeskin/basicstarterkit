﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicStarterKit.Models
{
    public class Product
        : BaseModel
    {
        public string ProductCode { get; set; }

        public string Name { get; set; }
        public string Detail { get; set; }
        public decimal Price { get; set; }
    }
}
