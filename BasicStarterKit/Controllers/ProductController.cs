﻿using BasicStarterKit.Common;
using BasicStarterKit.Models;
using BasicStarterKit.Services;
using Microsoft.AspNetCore.Mvc;

namespace BasicStarterKit.Controllers
{
    public class ProductController
        : BaseController
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        public IActionResult Index()
        {
            var inserted = productService.Add(new Product()
            {
                ActivePassive = ActivePassive.Active,
                ProductCode = "NE-123",
                Name = "Computer",
                Detail = "Lorem Inpuls",
                Price = 123
            });

            var list = productService.List();

            inserted.ProductCode = "RM-123";
            productService.Update(inserted);

            var list1 = productService.List();

            productService.Remove(inserted.Id);

            var list2 = productService.List();

            return View();
        }
    }
}